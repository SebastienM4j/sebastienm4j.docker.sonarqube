SonarQube Docker Image
======================

Image Docker de [SonarQube](https://www.sonarqube.org/) basée sur [l'image officielle](https://hub.docker.com/_/sonarqube).


Les plugins suivants sont pré-installés :

* sonar-java-plugin
* sonar-groovy-plugin
* sonar-cobertura-plugin
* sonar-jacoco-plugin
* sonar-go-plugin
* sonar-yaml-plugin
* sonar-kotlin-plugin
* sonar-css-plugin


Configuration
-------------

Une partie de la configuration est celle de [l'image officielle](https://hub.docker.com/_/sonarqube).

### Changement du mot de passe `admin`

Utiliser la variable d'environnement `SONARQUBE_CONFIG_ADMIN_PASSWORD` pour changer le mot de passe par défaut du compte `admin`.

Exemple :

```shell
docker run -p 9000:9000 -e SONARQUBE_CONFIG_ADMIN_PASSWORD=password sebastienm4j/sonarqube
```

### Nombre de tentative pour l'exécution du script de configuration

Le script de configuration utilise l'API de Sonarqube. Il est donc nécessaire que Sonar soit démarré.

Le script les lancé en arrière plan et effectue plusieurs tentatives de requête sur l'API jusqu'à ce que Sonar soit disponible. Une fois Sonar démarré, la configuration s'effectue.

Par défaut 60 tentatives avec 2 secondes entre chaques sont faites, soit 120 secondes de timeout.

Pour modifier le nombre de tentative, utiliser la variable d'environnement `SONARQUBE_CONFIG_TRIES`.

Exemple :

```shell
docker run -p 9000:9000 -e SONARQUBE_CONFIG_ADMIN_PASSWORD=password -e SONARQUBE_CONFIG_TRIES=30 sebastienm4j/sonarqube
```
