#!/usr/bin/env bash

set -e

if [ "${1:0:1}" != '-' ]; then
  exec "$@"
fi

# Parse Docker env vars to customize SonarQube
#
# e.g. Setting the env var sonar.jdbc.username=foo
# will cause SonarQube to be invoked with -Dsonar.jdbc.username=foo
#
# Or env var like SONARQUBE_CONFIG_*=value
# for the configuration script

declare -a sq_cfg
declare -a sq_opts

while IFS='=' read -r envvar_key envvar_value
do
    if [[ "$envvar_key" =~ SONARQUBE_CONFIG_.* ]]; then
        sq_cfg+=("${envvar_key} ${envvar_value} ")
    elif [[ "$envvar_key" =~ sonar.* ]]; then
        sq_opts+=("-D${envvar_key}=${envvar_value}")
    fi
done < <(env)

# Launch configuration script

echo "Lanch configuration script with parameters [${sq_cfg[@]}]"
./bin/config.sh $sq_cfg &

# Launch Sonarqube

exec java -jar lib/sonar-application-$SONAR_VERSION.jar \
  -Dsonar.log.console=true \
  -Dsonar.jdbc.username="$SONARQUBE_JDBC_USERNAME" \
  -Dsonar.jdbc.password="$SONARQUBE_JDBC_PASSWORD" \
  -Dsonar.jdbc.url="$SONARQUBE_JDBC_URL" \
  -Dsonar.web.javaAdditionalOpts="$SONARQUBE_WEB_JVM_OPTS -Djava.security.egd=file:/dev/./urandom" \
  "${sq_opts[@]}" \
  "$@"

